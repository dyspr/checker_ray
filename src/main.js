var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var dimension = 5
var initSize = 0.085

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < dimension; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize * 2, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize * 2)
      drawTile(boardSize * initSize, 255, sin(frameCount * 0.025 + (i + j * dimension)  * 0.025))
      pop()
    }
  }

  for (var i = 0; i < dimension - 1; i++) {
    for (var j = 0; j < dimension - 1; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5) + 0.5) * boardSize * initSize * 2, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5) + 0.5) * boardSize * initSize * 2)
      rotate(Math.PI)
      drawTile(boardSize * initSize, 255, sin(frameCount * 0.025 + (i + j * dimension) * 0.025))
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawTile(size, col, state) {
  fill(col)
  noStroke()
  beginShape()
  vertex((-1) * size * (3 / 6), (-1) * size * (3 / 6))
  vertex((-1) * size * (1 / 6), (-1) * size * (3 / 6))
  vertex((+1) * size * (1 / 6), (-1) * size * (3 / 6) + sin(state) * size * (2 / 6))
  vertex((+1) * size * (1 / 6), (-1) * size * (3 / 6))
  vertex((+1) * size * (3 / 6), (-1) * size * (3 / 6))
  vertex((+1) * size * (3 / 6), (-1) * size * (1 / 6))
  vertex((+1) * size * (3 / 6) + sin(state) * size * (2 / 6), (-1) * size * (1 / 6))
  vertex((+1) * size * (3 / 6), (+1) * size * (1 / 6))
  vertex((+1) * size * (3 / 6), (+1) * size * (3 / 6))
  vertex((+1) * size * (1 / 6), (+1) * size * (3 / 6))
  vertex((-1) * size * (1 / 6), (+1) * size * (3 / 6) + sin(state) * size * (2 / 6))
  vertex((-1) * size * (1 / 6), (+1) * size * (3 / 6))
  vertex((-1) * size * (3 / 6), (+1) * size * (3 / 6))
  vertex((-1) * size * (3 / 6), (+1) * size * (1 / 6))
  vertex((-1) * size * (3 / 6) + sin(state) * size * (2 / 6), (+1) * size * (1 / 6))
  vertex((-1) * size * (3 / 6), (-1) * size * (1 / 6))
  endShape()
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
